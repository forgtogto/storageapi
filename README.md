# JKSEO  파일업로드 다운로드 Api 

### 1. jar 파일 생성

```
mvn install
```

### 2. Dockerfile 빌드

```
sudo docker build -t storageapi/java .
```

### 3. 로컬에 저장할 볼륨 경로 폴더 생성
```
mkdir {폴더명}
```


### 4. Dockerfile 실행

```
sudo docker run -d -p 6070:6070 --name storageAPI -v /upload:/upload storageapi/java

```
