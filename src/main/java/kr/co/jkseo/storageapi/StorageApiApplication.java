package kr.co.jkseo.storageapi;

import kr.co.jkseo.storageapi.storage.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class StorageApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(StorageApiApplication.class, args);
    }

}
