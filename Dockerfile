FROM openjdk:11
ARG JAR_FILE=target/*.jar
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Asia/Seoul
COPY ${JAR_FILE} StorageApi.jar
ENTRYPOINT ["java","-jar","/StorageApi.jar"]